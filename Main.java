package com.company;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String kode;
        String jam;
        int harga=30000;
        Scanner input=new Scanner(System.in);
        Tiket user = new Tiket();
        System.out.println("Pemesanan Tiket Bioskop");
        System.out.println("Masukkan Nama : ");
        String nama = input.next();
        System.out.println("Masukkan Nomor Telepon : ");
        String nomor = input.next();

        user.setNama(nama);
        user.setTelepon(nomor);

        System.out.println("Pilihan Film : ");
        System.out.println("1. Avengers");
        System.out.println("2. Star Wars");
        System.out.println("3. Spiderman");
        System.out.println("Masukkan pilihan : ");
        int a = input.nextInt();
        if(a==1){
            kode = "A12";
        }else if(a==2) {
            kode = "A33";
        }else{
            kode = "B31";
        }
        System.out.println("Pilihan Jam Film : ");
        System.out.println("1. 15:00");
        System.out.println("2. 18:00");
        System.out.println("3. 20:00");
        System.out.println("Masukkan pilihan : ");
        int b = input.nextInt();
        if(b==1){
            jam = "15:00";
        }else if(b==2) {
            jam = "18:00";
        }else{
            jam = "20:00";
        }
        user.setKode(kode);
        user.setWaktu(jam);
        user.setHarga(harga);

        System.out.println("===============PEMESANAN===============");
        System.out.println("Customer Name\t\t: "+user.getNama());
        System.out.println("Customer Phone\t\t: "+user.getTelepon());
        System.out.println("=======================================");
        System.out.println("Kode Film\t\t: "+user.getKode());
        System.out.println("Jam Film\t\t: "+user.getWaktu());
        System.out.println("Harga Tiket\t\t: "+user.getHarga());
        System.out.println("=======================================");
        System.out.println("\n");
    }
}